
import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        // put your code here

        Scanner scanner = new Scanner(System.in);

        int N = scanner.nextInt();
        int M = scanner.nextInt();
        int K = scanner.nextInt();

        boolean cond1 = K <= N * M && K >= 0;
        boolean cond2 = K % N == 0;
        boolean cond3 = K % M == 0;

        if (cond1 && (cond2 || cond3)) {
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }
    }
}