
import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        // put your code here

        Scanner scanner = new Scanner(System.in);

        String line1 = scanner.nextLine();
        String line2 = scanner.nextLine();

        String newLine1 = line1.replaceAll("\\s+","");
        String newLine2 = line2.replaceAll("\\s+","");

        System.out.println(newLine1.equals(newLine2));

    }
}