import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // put your code here

        int days = scanner.nextInt();
        int foodPerDay = scanner.nextInt();
        int flight = scanner.nextInt();
        int hotelPerNight = scanner.nextInt();

        System.out.println(flight * 2 +
                days * foodPerDay +
                (days - 1) * hotelPerNight);
    }
}