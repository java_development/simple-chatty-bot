import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // put your code here

        int num = scanner.nextInt();
        int digit1 = num / 100;
        int digit2 = (num / 10) % 10;
        int digit3 = num % 10;

        int newNum = digit1 + 10 * digit2 + 100 * digit3;

        System.out.println(newNum);
    }
}