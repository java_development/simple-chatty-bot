import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // put your code here

        int height = scanner.nextInt();
        int upPerDay = scanner.nextInt();
        int downPerDay = scanner.nextInt();

        int days = (height - downPerDay - 1) / (upPerDay - downPerDay) + 1;

        System.out.println(days);
    }
}